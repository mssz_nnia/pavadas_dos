import sys

from PyQt5.QtWidgets import QMainWindow, QApplication

from PyQt5.QtSql import QSqlQuery, QSqlQueryModel

from PyQt5.QtCore import Qt

from conexion import crearConex

from tableview import Ui_MainWindow

class mainVentana(QMainWindow):

    def __init__(self):

        super(mainVentana, self).__init__()

        self.ui = Ui_MainWindow()

        self.ui.setupUi(self)

        modelo = QSqlQueryModel()

        query = QSqlQuery()

        query.prepare("SELECT * FROM articulo")

        query.exec_()

        modelo.setQuery(query)

        cabeceras = ['Codigo',
                     'Proveedor',
                     'Rubro',
                     'Articulo',
                     'Descripcion',
                     'Costo',
                     'Ganancia',
                     'IVA',
                     'Neto',
                     'Venta',
                     'Medida',
                     'Estado']

        for index, cabeceras in enumerate(cabeceras):

            modelo.setHeaderData(index, Qt.Horizontal, cabeceras)


        vista = self.ui.tableView
        vista.setModel(modelo)
       

if __name__ == "__main__":

    app = QApplication(sys.argv)

    if not crearConex():
        sys.exit(-1)

    ventana = mainVentana()

    ventana.show()

    sys.exit(app.exec_())