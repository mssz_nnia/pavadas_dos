
from PyQt5.QtWidgets import QMessageBox

from PyQt5.QtSql import QSqlDatabase

def crearConex():

    db = QSqlDatabase.addDatabase('QSQLITE')

    db.setDatabaseName('base.db')

    if not db.open():

        QMessageBox.critical(None,
                             "Error al abrir BD",
                             "\nClick Cancelar\n"
                             "para salir de esta ventana\n",
                             QMessageBox.Cancel)

        return False

    return True